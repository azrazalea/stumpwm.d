;;;; Copyright © 2013-2017 Lily Carpenter
;;;; All rights reserved.
;;;; Web: https://azrazalea
;;;; Email: azra-license@azrazalea.net

;;;; This config is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU Lesser General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.

;;;; This config is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU Lesser General Public License for more details.

;;;; You should have received a copy of the GNU Lesser General Public License
;;;; along with this config.  If not, see <http://www.gnu.org/licenses/>.
;;;;

;; -*-lisp-*-
(ql:quickload :slynk)
(in-package :stumpwm)

(load-module "cpu")
(load-module "mem")
(load-module "net")
(load-module "battery-portable")
(load-module "wifi")
(load-module "amixer")

(define-key *root-map* (kbd "c") "exec urxvt")

(defun utc-time (ml)
  (declare (ignore ml))
  (string-right-trim '(#\Newline)
                     (run-shell-command
                      "date -u --rfc-3339=seconds | cut -d '+' -f 1" t)))

(add-screen-mode-line-formatter #\U #'utc-time)

(defcommand emacsclient () ()
  (run-or-pull "emacsclient -c" '(:class "Emacs")))

(defcommand firefox () ()
  (run-or-pull "firefox" '(:title "Firefox|Iceweasel|IceCat")))

(defcommand chromium () ()
  (run-or-pull "chromium" '(:title "Chromium|Chrome")))

(define-key *root-map* (kbd "e") "emacsclient")

(define-key *root-map* (kbd "C-e") "exec emacs --daemon")

(define-key *root-map* (kbd "M-f") "firefox")

(define-key *root-map* (kbd "M-c") "chromium")

(define-key *root-map* (kbd "C-M-l") "exec xscreensaver-command -lock")

(setf stumpwm:*screen-mode-line-format*
      (let* ((base "%d | %U | %c | %M %N | %l")
             (battery-info (battery-portable::battery-info-string))
             (battery (unless (or (string= battery-info "(no battery)")
                                (string= battery-info "(no info)"))
                        " | %B"))
             (wifi (unless (or (string= (wifi::fmt-wifi nil) "No wireless device found.") (eq (wifi::fmt-wifi nil) nil))
                     " | %I ")))
        (concatenate 'string base battery wifi)))

(set-focus-color "purple")
(set-unfocus-color "grey")

(setf *mode-line-border-color* "black")
(setf *mode-line-foreground-color* "green")
(setf *mode-line-background-color* "black")

;; turn on/off the mode line for the current head only.
(stumpwm:toggle-mode-line (stumpwm:current-screen)
                          (stumpwm:current-head))

(define-key *root-map* (kbd "XF86AudioLowerVolume") "amixer-Master-1-")
(define-key *root-map* (kbd "XF86AudioRaiseVolume") "amixer-Master-1+")
(define-key *root-map* (kbd "XF86AudioMute") "amixer-Master-toggle pulse")

(slynk:create-server :port 7777
                     :dont-close t)
